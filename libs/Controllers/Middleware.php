<?php

namespace Wpk\job_id\Controllers;

use Wpk\job_id\Traits\Request;

/**
 * @author Przemysław Żydek
 */
abstract class Middleware {

	use Request;

}