<?php

namespace Wpk\job_id;
/**
 * Handles styles and scripts enqueue.
 *
 * @author Przemysław Żydek
 */
class Enqueue {

	/** @var string Stores url to assets folder */
	public $assetsUrl;

	/**@var string Stores url to css folder */
	public $cssUrl;

	/** @var string Stores url to js folder */
	public $jsUrl;

	/** @var string Stores url to vendor folder */
	public $vendorUrl;

	/**
	 * Enqueue constructor.
	 *
	 * @param string $assetsUrl URL to assets directory
	 */
	public function __construct( $assetsUrl = null ) {

		if ( empty( $assetsUrl ) ) {
			$core      = Core();
			$assetsUrl = "{$core->url}/assets";
		}

		$this->setupPaths( $assetsUrl );
		$this->setupHooks();

	}

	/**
	 * Setup project enqueue paths
	 *
	 * @param string $assetsUrl URL to assets directory
	 *
	 * @return void
	 */
	protected function setupPaths( $assetsUrl ) {

		$this->assetsUrl = $assetsUrl;
		$this->cssUrl    = $this->getAssetsPath( 'css' );
		$this->jsUrl     = $this->getAssetsPath( 'js' );
		$this->vendorUrl = $this->getAssetsPath( 'vendor' );

	}

	/**
	 * Helper function for getting assets path
	 *
	 * @param string $path
	 *
	 * @return string
	 */
	public function getAssetsPath( $path ) {

		return "{$this->assetsUrl}/$path";

	}

	/**
	 * Setup class hooks
	 *
	 * @return void
	 */
	protected function setupHooks() {

		add_action( 'wp_enqueue_scripts', [ $this, 'enqueueStyles' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'dequeueScripts' ] );
		add_action( 'wp_print_scripts', [ $this, 'dequeueScripts' ] );

		//Admin enqueue
		add_action( 'admin_enqueue_scripts', [ $this, 'adminEnqueueScripts' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'adminEnqueueStyles' ] );
		add_action( 'admin_print_scripts', [ $this, 'adminDequeueScripts' ] );

	}

	/**
	 * Enqueue styles
	 *
	 * @return void
	 */
	public function enqueueStyles() {


	}

	/**
	 * Enqueue admin styles
	 *
	 * @return void
	 */
	public function adminEnqueueStyles() {


	}

	/**
	 * Enqueue scripts
	 *
	 * @return void
	 */
	public function enqueueScripts() {


	}

	/**
	 * Enqueue admin scripts
	 *
	 * @return void
	 */
	public function adminEnqueueScripts() {


	}

	/**
	 * Helper function for enqueueing styles
	 *
	 * @param array args {
	 *
	 * @type string     $slug
	 * @type string     $fileName
	 * @type array      $deps
	 * @type string|int $ver
	 *
	 * }
	 *
	 * @return self
	 */
	protected function enqueueStyle( $args = [] ) {

		$args = wp_parse_args( $args, [
			'slug'     => CORE::SLUG,
			'fileName' => Core::SLUG,
			'deps'     => [],
			'ver'      => '1.0',
		] );

		extract( $args );

		/**
		 * @var string     $slug
		 * @var string     $fileName
		 * @var array      $deps
		 * @var string|int $ver
		 */

		$url = $this->cssUrl;


		if ( ! Utility::contains( $fileName, 'http' ) ) {
			$fileName = str_replace( '.css', '', $fileName );
			$fileName = "$url/$fileName.css";
		}

		wp_enqueue_style( $slug, $fileName, $deps, $ver );

		return $this;

	}

	/**
	 * Helper function for enqueuing scripts
	 *
	 * @param array $args
	 *
	 * @return self
	 */
	protected function enqueueScript( $args = [] ) {

		$args = wp_parse_args( $args, [
			'slug'     => CORE::SLUG,
			'fileName' => Core::SLUG,
			'deps'     => [ 'jquery' ],
			'ver'      => '1.0',
			'inFooter' => false,
			'vars'     => [],
		] );

		extract( $args );

		/**
		 * @var string $slug
		 * @var string $fileName
		 * @var array  $deps
		 * @var string $ver
		 * @var bool   $inFooter
		 * @var array  $vars
		 */

		$url = $this->jsUrl;

		if ( ! Utility::contains( $fileName, 'http' ) ) {
			$fileName = str_replace( '.js', '', $fileName );
			$fileName = "$url/$fileName.js";
		}

		wp_enqueue_script( $slug, $fileName, $deps, $ver, $inFooter );

		if ( ! empty( $vars ) ) {
			wp_localize_script(
				$slug,
				//Fix issue with invalid variable when "-" is present
				sprintf( '%s_vars', str_replace( '-', '_', $slug ) ),
				$vars );
		}

		return $this;

	}

	/**
	 * Dequeues scripts.
	 *
	 * @return void
	 */
	public function dequeueScripts() {
	}

	/**
	 * Dequeues admin scripts.
	 *
	 * @return void
	 */
	public function adminDequeueScripts() {

	}

}

