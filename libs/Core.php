<?php

namespace Wpk\job_id;

/**
 * Core class
 *
 * @author Przemysław Żydek
 */
final class Core {

	/** @var Core Stores class instance */
	private static $instance;

	/** @var string Slug used for translations */
	const SLUG = 'wpk_job_id';

	/** @var string Stores path to this plugin directory */
	public $dir;

	/** @var string Stores url to this plugin directory */
	public $url;

	/** @var string Main plugin file */
	public $file;

	/** @var Loader Stores loader instance */
	public $loader;

	/** @var Utility */
	public $utility;

	/** @var Enqueue */
	public $enqueue;

	/** @var View */
	public $view;

	/**
	 * Core constructor.
	 */
	private function __construct() {
	}

	/**
	 * No serializing
	 */
	private function __sleep() {
	}

	/**
	 * No unserializing
	 */
	private function __wakeup() {
	}

	/**
	 * Core init
	 *
	 * @param string $file Main plugin file
	 * @param string $namespace Namespace of core class
	 *
	 * @return Core
	 */
	public function init( $file = null, $namespace = null ) {

		$this->file = empty( $file ) ? __FILE__ : $file;
		$this->url  = plugin_dir_url( $this->file );
		$this->dir  = plugin_dir_path( $this->file );

		$namespace = empty( $namespace ) ? __NAMESPACE__ : $namespace;

		$this->loader  = new Loader( $this->dir . '/libs', $namespace );
		$this->enqueue = new Enqueue();
		$this->utility = new Utility();
		$this->view    = new View( $this->getPath( 'views' ) );

		return $this;

	}

	/**
	 * @param string $path
	 *
	 * @return string
	 */
	public function getPath( $path = '' ) {
		return "{$this->dir}/{$path}";
	}

	/**
	 * Get Core instance.
	 *
	 * @return Core
	 */
	public static function getInstance() {

		if ( empty( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;

	}
}
