<?php


namespace Wpk\job_id\Interfaces;

/**
 * Converts object to json
 */
interface Jsonable {

	/**
	 * @return string
	 */
	public function toJson();

}