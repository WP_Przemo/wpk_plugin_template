<?php


namespace Wpk\job_id\Interfaces;

/**
 * Converts object to array
 */
interface Arrayable {

	/**
	 * @return array
	 */
	public function toArray();


}