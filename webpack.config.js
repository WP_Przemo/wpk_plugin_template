const path = require( 'path' );

module.exports = {
	entry:     './src/js/main.js',
	output:    {
		filename: 'wpk-job-id.js',
		path:     path.resolve( __dirname, 'assets/js' )
	},
	module:    {
		rules: [
			{
				test:    /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use:     {
					loader:  'babel-loader',
					options: {
						presets: [ '@babel/preset-env' ],
					}
				}
			},
			{
				test: /\.(s*)css$/,
				use:  [ 'style-loader', 'css-loader', 'sass-loader' ]
			}
		]
	},
	externals: {
		jquery: 'jQuery'
	}
};